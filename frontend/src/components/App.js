import React, { Component, Fragment } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'


import Footer from './Footer'
import Header from './Header'

// import Menu from './Menu'
import Register from './Register'
import Login from './Login'
import Logout from './Logout'
import Booking from './Booking'
import List from './List'
import Product from './Product'
import ProductList from './ProductList'
import ProductUpdate from './ProductUpdate'


class App extends Component {

	constructor(props) {
		super(props)

		this.state = {
			name: localStorage.getItem('name'),
			role: localStorage.getItem('role'),
			token: localStorage.getItem('token'),
			cartQuantity: 0
		}
	}

	setUser() {
		this.setState({
			name: localStorage.getItem('name'),
			role: localStorage.getItem('role'),
			token: localStorage.getItem('token')
		})
	}

	unsetUser() {
		localStorage.clear()

		this.setState({
			name: localStorage.getItem('name'),
			role: localStorage.getItem('role'),
			token: localStorage.getItem('token')
		})
	}



	render() {
		let LoginComponent = (props) => (<Login {...props} setUser={ this.setUser.bind(this) } cartQuantity={this.state.cartQuantity} />)
		let LogoutComponent = (props) => (<Logout {...props} unsetUser={ this.unsetUser.bind(this) }/>)
		// let MenuComponent = (props) => (<Menu {...props} />)
		let BookingComponent = (props) => (<Booking {...props} />)
		let ListComponent = (props) => (<List {...props} userId={this.state}/>)
		let ProductComponent = (props) => (<Product {...props} />)
		let ProductListComponent = (props) => (<ProductList {...props} />)
		   
		return (
			<Fragment>
					<BrowserRouter>
            
                    <Header token={ this.state.token } name={ this.state.name } role={ this.state.role } cartQuantity={ this.state.cartQuantity}/>
						
							<Switch>
								{/* <Route exact path='/' render={ MenuComponent }/> */}
								<Route exact path='/register' component={ Register }/>
								<Route exact path='/login' render={ LoginComponent }/>
								<Route exact path='/logout' render={ LogoutComponent }/>
								<Route exact path='/booking' component={ BookingComponent }/>
								<Route exact path='/list' component={ ListComponent }/>
								<Route exact path='/product' component={ ProductComponent }/>
								<Route exact path='/product-list' component={ ProductListComponent }/>
								<Route exact path='/product-update' component={ ProductUpdate }/>
							</Switch>
					</BrowserRouter>

					<Footer/>
			</Fragment>
			)
	}

}

export default App