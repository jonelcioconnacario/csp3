import React from 'react'
import { Link } from 'react-router-dom'

const Header = (props) => {

	let navRight = (localStorage.getItem('token') === null) ?
		(
			<React.Fragment>
				<li className="nav-item">
					<Link className="nav-link text-white" to="/login">Login</Link>
				</li>
				<li className="nav-item">
					<Link className="nav-link text-white" to="/register">Register</Link>
				</li>
			</React.Fragment>
		) :
		(
			<React.Fragment>
				<li className="nav-item">
					<Link className="nav-link text-white" to="/">{localStorage.getItem('name')}</Link>
				</li>
				<li className="nav-item">
					<Link className="nav-link text-white" to="/logout">Logout</Link>
				</li>

			</React.Fragment>
		)

	let navLeft = (localStorage.getItem('role') === "customer") ?
		(
			<React.Fragment>
				<li className="nav-item">
					<Link className="nav-link text-white" to="/booking">Schedule a pick-up</Link>
				</li>

				<li className="nav-item">
					<Link className="nav-link text-white" to="/list">List of Booked</Link>
				</li>
			</React.Fragment>
		) :
		(
			<React.Fragment>
				<li className="nav-item">
					<Link className="nav-link text-white" to="/product-list">Product List</Link>
				</li>
				<li className="nav-item">
					<Link className="nav-link text-white" to="/product">Add New Product</Link>
				</li>
			</React.Fragment>
		)

	return (
		<nav className="navbar navbar-expand-lg bg-info fixed-top" id="navbar-style">

			<Link className="navbar-brand text-dark" to="/">Online Delivery Booking</Link>

			<button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar">
				<span className="navbar-toggler-icon"></span>
			</button>

			<div className="collapse navbar-collapse" id="navbar">
				<ul className="navbar-nav mr-auto">
					
					{
					(localStorage.getItem('token') === null) ?
					(
						<React.Fragment></React.Fragment>
					):
					(
						<React.Fragment>{navLeft}</React.Fragment>
					)
					}
				</ul>

				<ul className="navbar-nav ml-auto">
					{navRight}
				</ul>
			</div>

		</nav>
	)
}

export default Header