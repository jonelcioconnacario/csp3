import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import api from '../api-proxy'

const List = () => (

    <div className="container mt-5 pt-5">
        <table className="table table-hover">
        <thead className="btn-primary">
            <tr>
            <th scope="col">Tracking No</th>
            <th scope="col">Cosignee Name</th>
            <th scope="col">Date Booked</th>
            <th scope="col">Item Description</th>
			<th>Amount</th>
            </tr>
        </thead>
            <ListTable />
        </table>
    </div>
)

class ListTable extends Component {

    constructor(props) {
        super(props)

        this.state = {
            items: []
        }
    }

    componentWillMount() {
        fetch(api.url + '/booking/all')
            .then((response) => response.json())
            .then((lists) => {
                console.log(lists)
                this.setState({ items: lists })
            })
     }

render() {

	return (
        <tbody>
			{
				this.state.items.map((item) => {
					return ( 
						<tr key={item._id}>
							<th>{item._id}</th>
							<th>{item.cosgineeName}</th>
							<th></th>
							<th>{item.itemDescription}</th>
							<th>{item.amount}</th>
						</tr>
					)
				})
			}	
      </tbody>
		)
}

}

export default List