import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import api from '../api-proxy'

const Register = () => (

<div className="container-fluid pt-5">

	<div className="row">

		<div className="col-6 mx-auto mt-5">

			<h1 className="text-center mb-3">Registration Page</h1>

			<div className="card">

				<div className="card-header">Registration information</div>

				<div className="card-body">

					<RegisterForm />

				</div>

			</div>

		</div>

	</div>

</div>	
)

class RegisterForm extends Component {

	constructor(props) {
		super(props)

		this.state = {
			name: '',
			email: '',
			password: '',
			gotoLogin: false,
			isSubmitDisable: true
		}
	}

	nameChangeHandler(e) {
		this.setState({ name: e.target.value })
	}

	emailChangeHandler(e){
		this.setState({ email: e.target.value })
	}

	passwordChangeHandler(e){

		if (e.target.value.length < 8) {
			this.setState({isSubmitDisable: true})
		} else {
			this.setState({isSubmitDisable: false})
		}

		this.setState({ password: e.target.value })
	}

	formSubmitHandler(e){
		e.preventDefault()

		// let formData = new FormData()

		// formData.append('name', this.state.name)
		//formData.append('email', this.state.email)
		//formData.append('password', this.state.password)

		let payload =  {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: this.state.name,
				email: this.state.email,
				password: this.state.password
			})
		}

		fetch(api.url + '/user/register', payload)
		// .then(function(response) { return response.json() }) 
		.then((response) => response.json()) // <-- shorter version nung sa taas
		.then((response) => {
			if (response.error != null) {
				alert(response.error)
			} else {
				this.setState({ gotoLogin: true })
			}
		})

}




render() {

	if (this.state.gotoLogin) {
		return <Redirect to='/login?register_success=true'/>
	}
	return (

		<form onSubmit={ this.formSubmitHandler.bind(this) }>

		<div className="form-group">

		<label>Name</label>
		<input defaultValue={ this.state.name } onChange={ this.nameChangeHandler.bind(this) } type="text" className="form-control mb-1"/>
		<span className="text-danger mt-2"></span>

		</div>

		<div className="form-group">

		<label>Email</label>
		<input defaultValue={ this.state.email } onChange={ this.emailChangeHandler.bind(this) } type="email" placeholder="sample@gmail.com" className="form-control mb-1"/>
		<span className="text-danger mt-2"></span>

		</div>

		<div className="form-group">

		<label className="mt-2">Password</label>
		<input defaultValue={ this.state.password } onChange={ this.passwordChangeHandler.bind(this) } type="password" className="form-control mb-1"/>
		<span className="text-danger mt-2" defaultValue={ this.state.message }></span>

		</div>

		<button disabled={ this.state.isSubmitDisable } type="submit" className="btn btn-success btn-block">Register</button>

		</form>

		)
}

}

export default Register