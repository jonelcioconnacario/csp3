import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import api from '../api-proxy'
import { Link } from 'react-router-dom'

const ProductList = () => (

    <div className="container mt-5 pt-5">
        <table className="table table-sm">
            <thead className="btn-secondary">
                <tr>
                    <th scope="col">Product ID</th>
                    <th scope="col">Product Name</th>
                    <th scope="col">Small Amount</th>
                    <th scope="col">Medium Amount</th>
                    <th scope="col">Large Amount</th>
                    <th>Action</th>
                </tr>
            </thead>
            <ProductListForm />
        </table>
    </div>
)

class ProductListForm extends Component {

    constructor(props) {
        super(props)

        this.state = {
            items: []
        }
    }

    componentWillMount() {
        fetch(api.url + '/product/all')
            .then((response) => response.json())
            .then((products) => {
                console.log(products)
                this.setState({ items: products })
            })

    }

    render() {

        return (
            <tbody>
                {
                    this.state.items.map((item) => {
                        return (
                            <tr key={item._id}>
                                <th>{item._id}</th>
                                <th>{item.ProductName}</th>
                                <th>{item.SmallAmount}</th>
                                <th>{item.MediumAmount}</th>
                                <th>{item.LargeAmount}</th>
                                <th>
                                    <div className="btn-group">
                                     <React.Fragment>
                                        <Link className="btn btn-sm btn-outline-primary mr-2 float-left" to={"/product-update?id=" + item._id} >Update</Link>
                                    </React.Fragment>
                                    <DeleteProduct itemId={item._id} />
                                    </div>
                                </th>
                            </tr>
                        )
                    })
                }
            </tbody>
        )
    }
}

class DeleteProduct extends Component {

	constructor(props) {
		super(props)

		this.state = {
			returnProductList: false
		}
	}

    fromSubmitHandler(e){
        e.preventDefault();
        
        let payload = {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				'_id': this.props.itemId
			})
		}

		fetch(api.url + '/product/delete', payload)
		.then((response) => response.json())
		.then((response) => {
			if (response.error != null) {
				alert(response.error)
			} else {
				this.setState({ returnProductList: true })
			}
		})
    }

    render(){

        if (this.state.returnProductList) {
			return <Redirect to='/product-list'/>
		}

        return (
            <form  onSubmit={ this.fromSubmitHandler.bind(this) }>
            <div className="modal" id={ 'modal-' + this.props.itemId } tabIndex="-1" role="dialog">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Delete Confirmation</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p className="text-danger">Are you sure you want to DELETE this Employee ?</p>
                            <h5><b className="text-danger">{this.props.itemId}</b></h5>
                        </div>
                        <div className="modal-footer">
                            <button type="submit" className="btn btn-primary" id="secondModalYes" >Yes</button>
                            <button type="button" className="btn btn-secondary"  data-dismiss="modal" id="secondModalClose">No</button>
                        </div>
                    </div>
                </div>
            </div>
            <button type="button" className="btn btn-sm btn btn-outline-danger" data-toggle="modal" data-target={ '#modal-' + this.props.itemId }>
                Delete
            </button>
        </form>
        )
    }
}

export default ProductList