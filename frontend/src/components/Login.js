import React, { Component } from 'react'
import queryString from 'query-string'
import { Redirect, Link } from 'react-router-dom'
import api from '../api-proxy'


document.title = 'Login'

const Login = (props) => {

	if (localStorage.getItem('token') != null) {
		
		if (localStorage.getItem('role') === "admin"){
			return <Redirect to='/product-list'/>
		}else{
			return <Redirect to='/list'/>
		}
	}

	return (

		<div className="row">
			
			<div className="col-6 mx-auto mt-5 pt-5">

				<h1 className="text-center"><b>Login Page</b></h1>


				<div className="card">
					
					<div className="card-header">Enter login information</div>

					<div className="card-body">

						<LoginForm urlParam={ props.location.search } setUser={ props.setUser}/>

					</div>


				</div>

			</div>

		</div>

	)
}

class LoginForm extends Component {

constructor(props){
		super (props)

		this.state = {
			email: '',
			password: '',
			errorMessage: '',
			gotoMenu: false
		}
	}

emailChangeHandler(e) {
	this.setState ({ email: e.target.value })
}

passwordChangeHandler(e) {
	this.setState ({ password: e.target.value })
}

formSubmitHandler(e) {
	e.preventDefault()
	


	let payload = {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		}, 
		body: JSON.stringify({
			email: this.state.email,
			password: this.state.password
		})
	}

	fetch(api.url + '/user/login', payload)
	.then((response) => response.json())
	.then((response) => {
		console.log(response)
		if (response.result === 'authenticated') {
			localStorage.setItem('role', response.role)
			localStorage.setItem('name', response.name)
			localStorage.setItem('token', response.token)
			this.props.setUser()
			this.setState({
				gotoMenu: true
			})
		} else {
			this.setState({
				errorMessage: <div className="alert alert-danger">{response.error}</div>
			})
		}
		// this.setState({gotoMenu: true})
	})
}

	render() {

		if (this.state.gotoMenu) {
			return <Redirect to='/booking'/>
		}

		let url = this.props.urlParam
		let param = queryString.parse(url)
		let registerSuccessMessage = null
		let message = ''

		if(param.register_success) {
			registerSuccessMessage = <div className="alert alert-success">Register successful, you may now login</div>
		}

		if (this.state.errorMessage == '' && registerSuccessMessage != null) {
			message = registerSuccessMessage
			} else {
				message = this.state.errorMessage
			}

		return (

			<form onSubmit={ this.formSubmitHandler.bind(this) }>

			{ message }

				<label >Email</label>
				<input value={ this.state.email } onChange={ this.emailChangeHandler.bind(this) } type="text" className="form-control mb-4"/>

				<label htmlFor="txt-password">Password</label>
				<input value={this.state.password} onChange={ this.passwordChangeHandler.bind(this) } type="password" className="form-control mb-4"/>

				<button type="submit" className="btn btn-success btn-block">Login</button>

			</form>

		)
	}
}

export default Login