import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import api from '../api-proxy'


const Register = () => (

    <div className="container-fluid">

        <div className="row">

            <div className="col-md-12 mx-auto mt-5 pt-5">

                <h1 className="mb-3">Booking Page</h1>

                <div className="card">

                    <div className="card-header">Booking information</div>

                    <div className="card-body">

                        <RegisterForm />

                    </div>

                </div>

            </div>

        </div>

    </div>
)

class RegisterForm extends Component {

    constructor(props) {
        super(props)

        this.state = {
            user: [],
            product: [],
            size: {
                small: '',
                medium: '',
                large: ''
            },
            productId: undefined,
            amount: undefined,
            shipperName: localStorage.getItem('name'),
            cosgineeName: '',
            cosigneeNumber: '',
            streetName: '',
            barangay: '',
            cityMuni: '',
            regionProvince: '',
            zipcode: '',
            itemDescription: '',
            specialInstruction: '',
            returnToList: false
        }
    }

    componentWillMount() {

        fetch(api.url + '/product/all')
            .then((response) => response.json())
            .then((products) => {
                console.log(products)
            })
    }

    productIdCH(e) {
        this.setState({ productId: e.target.value })

        let x = e.target.value
        let c = this.state.product.length
        let s = undefined
        let m = undefined
        let l = undefined

        for (let i = 0; i < c; i++) {
            if (x === this.state.product[i]._id) {
                s = this.state.product[i].SmallAmount
                m = this.state.product[i].MediumAmount
                l = this.state.product[i].LargeAmount
            }
        }

        this.setState({
            size: {
                small: s,
                medium: m,
                large: l
            }
        })
    }

    shipperNameCH(e) {this.setState({ shipperName: e.target.value })}
    amountCH(e) {this.setState({ amount: e.target.value })}
    cosgineeNameCH(e) {this.setState({ cosgineeName: e.target.value })}
    cosigneeNumberCH(e) {this.setState({ cosigneeNumber: e.target.value })}
    streetNameCH(e) {this.setState({ streetName: e.target.value })}
    barangayCH(e) {this.setState({ barangay: e.target.value })}
    cityMuniCH(e) {this.setState({ cityMuni: e.target.value })}
    regionProvinceCH(e) {this.setState({ regionProvince: e.target.value })}
    zipcodeCH(e) {this.setState({ zipcode: e.target.value })}
    itemDescriptionCH(e) {this.setState({ itemDescription: e.target.value })}
    specialInstructionCH(e) {this.setState({ specialInstruction: e.target.value })}

    proceedToCheckout() {
        console.log(this.state.cosgineeName)
        let payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem('token')
            },
            body: JSON.stringify({
                shipperName: this.state.shipperName,
                totalPrice: this.state.amount,
                cosgineeName: this.state.cosgineeName,
                cosigneeNumber: this.state.cosigneeNumber,
                streetName: this.state.streetName,
                barangay: this.state.barangay,
                cityMuni: this.state.cityMuni,
                regionProvince: this.state.regionProvince,
                zipcode: this.state.zipcode,
                itemDescription: this.state.itemDescription,
                specialInstruction: this.state.specialInstruction,
                productId: this.state.productId
            })
        }

        fetch(api.url + '/booking/create', payload)
            .then((response) => response.json())
            .then((response) => {
                if (response.error != null) {
                    console.log(response.error)
                } else {
                    alert("Booking Success")
                    this.setState({ returnToList: true })
                }
            })
    }

    proceedToStripeCheckout() {

        let payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem('token')
            },
            body: JSON.stringify({
                shipperName: this.state.shipperName,
                totalPrice: this.state.amount,
                cosgineeName: this.state.cosgineeName,
                cosigneeNumber: this.state.cosigneeNumber,
                streetName: this.state.streetName,
                barangay: this.state.barangay,
                cityMuni: this.state.cityMuni,
                regionProvince: this.state.regionProvince,
                zipcode: this.state.zipcode,
                itemDescription: this.state.itemDescription,
                specialInstruction: this.state.specialInstruction,
                productId: this.state.productId
            })
        }

        fetch(api.url + '/booking/stripeCheckout', payload)
            .then((response) => response.json())
            .then((response) => {
                if (response.error != null) {
                    console.log(response.error)
                } else {
                    alert("success")
                    this.setState({ returnToList: true })
                }
            })
    }

    render() {

        if (this.state.returnToList) {
			return <Redirect to='/list'/>
		}

        return (
            // consignor
            // consignee
            // <textarea class="form-control mb-3" rows="2"></textarea>
            <form>
                <div className="form-row">
                    <div className="form-group col-md-4">
                        <label>Tracking Number</label>
                        <input className="form-control" type="text" disabled />
                    </div>
                    <div className="form-group col-md-4">
                        <label>Shipper Name</label>
                        <input defaultValue={this.state.shipperName} onChange={this.shipperNameCH.bind(this)} className="form-control" type="text" disabled />
                    </div>

                </div>
                <hr></hr>
                <div className="form-row">
                    <div className="form-group col-md-4">
                        <label>Consignee Name</label>
                        <input defaultValue={this.state.cosgineeName} onChange={this.cosgineeNameCH.bind(this)} className="form-control" type="text" />
                    </div>
                    <div className="form-group col-md-4">
                        <label>Contanct Number</label>
                        <input defaultValue={this.state.cosigneeNumber} onChange={this.cosigneeNumberCH.bind(this)} className="form-control" type="text" />
                    </div>
                    <div className="form-group col-md-2">
                        <label id="sel1">Products</label>
                        <select defaultValue={this.state.productId} onChange={this.productIdCH.bind(this)} className="form-control" id="sel1">
                            <option>Select Product</option>
                            {
                                this.state.product.map((product) => {
                                    return <option key={product._id} value={product._id}>{product.ProductName}</option>
                                })
                            }
                        </select>
                    </div>
                    <div className="form-group col-md-2">
                        <label id="sel1">Size</label>
                        <select defaultValue={this.state.amount} onChange={this.amountCH.bind(this)} className="form-control" id="sel2">
                            <option value="0">Choose one..</option>
                            <option value={this.state.size.small}>Small: &#8369; {this.state.size.small}</option>
                            <option value={this.state.size.medium}>Medium  &#8369; {this.state.size.medium}</option>
                            <option value={this.state.size.large}>Large  &#8369; {this.state.size.large}</option>
                        </select>
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-4">
                        <label>House #, Street Name</label>
                        <input defaultValue={this.state.streetName} onChange={this.streetNameCH.bind(this)} className="form-control" type="text" />
                    </div>
                    <div className="form-group col-md-2">
                        <label>Barangay</label>
                        <input defaultValue={this.state.barangay} onChange={this.barangayCH.bind(this)} className="form-control" type="text" />
                    </div>
                    <div className="form-group col-md-2">
                        <label>City / Municipality</label>
                        <input defaultValue={this.state.cityMuni} onChange={this.cityMuniCH.bind(this)} className="form-control" type="text" />
                    </div>
                    <div className="form-group col-md-2">
                        <label>Region / Province</label>
                        <input defaultValue={this.state.regionProvince} onChange={this.regionProvinceCH.bind(this)} className="form-control" type="text" />
                    </div>
                    <div className="form-group col-md-2">
                        <label>Zip Code</label>
                        <input defaultValue={this.state.zipcode} onChange={this.zipcodeCH.bind(this)} className="form-control" type="text" />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <label>Item Description</label>
                        <textarea defaultValue={this.state.itemDescription} onChange={this.itemDescriptionCH.bind(this)} className="form-control mb-3" rows="2"></textarea>
                    </div>
                    <div className="form-group col-md-6">
                        <label>Special Instruction</label>
                        <textarea defaultValue={this.state.specialInstruction} onChange={this.specialInstructionCH.bind(this)} className="form-control mb-3" rows="2"></textarea>
                    </div>
                </div>
                <div>
                    <b >Amount: &#8369; {this.state.amount} </b>
                </div>
                <br></br>
                <button onClick={this.proceedToCheckout.bind(this)} type="button" className="btn btn-primary mr-3">Proceed to Checkout (CASH)</button>
                <button onClick={this.proceedToStripeCheckout.bind(this)} type="button" className="btn btn-primary mr-3">Proceed to Checkout (Stripe)</button>
            </form>

        )
    }

}

export default Register