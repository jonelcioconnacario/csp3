import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import api from '../api-proxy'

document.title = 'Create Product'

const Product = () => (
    <div className="container-fluid mt-5 ">

        <div className="row mt-5">

            <div className="col-8 mx-auto mt-5">

                <h3 className="text-center">Add Product</h3>

                <div className="card">

                    <div className="card-header">Product Imformation</div>

                    <div className="card-body">

                        <ProductForm />

                    </div>

                </div>

            </div>

        </div>

    </div>
)



class ProductForm extends Component {

    constructor(props) {
        super(props)

        this.state = {
            ProductName: '',
            SmallAmount: '',
            MediumAmount: '',
            LargeAmount: '',
            gotoProduct: false,
            isSubmitDisable: true
        }
    }

    
    ProductNameCH(e) {
        this.setState({ProductName: e.target.value})
    }

    SmallAmountCH(e){
        this.setState({SmallAmount: e.target.value})
    }

    MediumAmountCH(e){
        this.setState({MediumAmount: e.target.value})
    }

    LargeAmountCH(e){
        this.setState({LargeAmount: e.target.value})
    }

	formSubmitHandler(e){
		e.preventDefault()

		let formData = new FormData()

		formData.append('ProductName', this.state.ProductName)
		formData.append('SmallAmount', this.state.SmallAmount)
        formData.append('MediumAmount', this.state.MediumAmount)
        formData.append('LargeAmount', this.state.LargeAmount)

		let payload =  {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
                ProductName: this.state.ProductName,
                SmallAmount: this.state.SmallAmount,
                MediumAmount: this.state.MediumAmount,
                LargeAmount: this.state.LargeAmount,
                isArchived: 0
			})
		}

		fetch(api.url + '/product/create', payload)
		// .then(function(response) { return response.json() }) 
		.then((response) => response.json()) // <-- shorter version nung sa taas
		.then((response) => {
			if (response.error != null) {
				alert(response.error)
			} else {
                this.setState({ gotoProduct: true })
                alert('Product Added')
                this.setState({
                    ProductName: '',
                    SmallAmount: '',
                    MediumAmount: '',
                    LargeAmount: ''
                })
			}
		})

}

    render() {


        return (

            <form onSubmit={ this.formSubmitHandler.bind(this) }>
                <div className="form-group">
                    <label >Product Name</label>
                    <input defaultValue={ this.state.ProductName } onChange={ this.ProductNameCH.bind(this) }  type="text" className="form-control" />
                </div>

                <div className="mb-3">
                    <b>Size and Amount</b>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Small</label>
                    <div className="col-sm-10">
                        <input defaultValue={ this.state.SmallAmount } onChange={ this.SmallAmountCH.bind(this) } type="number" className="form-control" placeholder="Amount" />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Medium</label>
                    <div className="col-sm-10">
                        <input defaultValue={ this.state.MediumAmount } onChange={ this.MediumAmountCH.bind(this) } type="number" className="form-control" placeholder="Amount" />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Large</label>
                    <div className="col-sm-10">
                        <input defaultValue={ this.state.LargeAmount } onChange={ this.LargeAmountCH.bind(this) } type="number" className="form-control" placeholder="Amount" />
                    </div>
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>

        )
    }

}

export default Product