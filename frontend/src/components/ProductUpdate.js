import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import api from '../api-proxy'
import queryString from 'query-string'

document.title = 'Create Product'

const ProductUpdate = (props) => (
    <div className="container-fluid mt-5 pt-5">

        <div className="row mt-5">

            <div className="col-8 mx-auto mt-5">

                <h3 className="text-center">Add Product</h3>

                <div className="card">

                    <div className="card-header">Product Imformation</div>

                    <div className="card-body">

                        <ProductUpdateForm urlParam={props.location.search} />

                    </div>

                </div>

            </div>

        </div>

    </div>
)



class ProductUpdateForm extends Component {

    constructor(props) {
        super(props)

        let params = queryString.parse(this.props.urlParam)

        this.state = {
            _id: params.id,
            ProductName: '',
            SmallAmount: '',
            MediumAmount: '',
            LargeAmount: '',
            gotoProduct: false,
            isSubmitDisable: true
        }
    }

    componentWillMount() {
        fetch(api.url + '/product/update/' + this.state._id)
            .then((response) => response.json())
            .then((product) => {
                console.log(product)
                this.setState({
                    ProductName: product.ProductName,
                    SmallAmount: product.SmallAmount,
                    MediumAmount: product.MediumAmount,
                    LargeAmount: product.LargeAmount
                })
            })
    }

    ProductNameCH(e) {
        this.setState({ ProductName: e.target.value })
    }

    SmallAmountCH(e) {
        this.setState({ SmallAmount: e.target.value })
    }

    MediumAmountCH(e) {
        this.setState({ MediumAmount: e.target.value })
    }

    LargeAmountCH(e) {
        this.setState({ LargeAmount: e.target.value })
    }

    formSubmitHandler(e) {
        e.preventDefault()

        let payload = {
            method: 'put',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                _id: this.state._id,
                ProductName: this.state.ProductName,
                SmallAmount: this.state.SmallAmount,
                MediumAmount: this.state.MediumAmount,
                LargeAmount: this.state.LargeAmount,
                isArchived: 0
            })
        }

		fetch(api.url + '/product', payload)
		.then((response) => response.json())
		.then((response) => {
			if (response.error != null) {
				alert(response.error)
			} else {
                alert(this.state._id + " is successfully updated !")
                this.setState({ gotoProduct: true })
			}
		})
	}

    render() {

        if (this.state.gotoProduct) {
            return <Redirect to='/product-list' />
        }

        return (

            <form onSubmit={this.formSubmitHandler.bind(this)}>
                <div className="form-group">
                    <label >Product Name</label>
                    <input defaultValue={this.state.ProductName} onChange={this.ProductNameCH.bind(this)} type="text" className="form-control" />
                </div>

                <div className="mb-3">
                    <b>Size and Amount</b>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Small</label>
                    <div className="col-sm-10">
                        <input defaultValue={this.state.SmallAmount} onChange={this.SmallAmountCH.bind(this)} type="number" className="form-control" placeholder="Amount" />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Medium</label>
                    <div className="col-sm-10">
                        <input defaultValue={this.state.MediumAmount} onChange={this.MediumAmountCH.bind(this)} type="number" className="form-control" placeholder="Amount" />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Large</label>
                    <div className="col-sm-10">
                        <input defaultValue={this.state.LargeAmount} onChange={this.LargeAmountCH.bind(this)} type="number" className="form-control" placeholder="Amount" />
                    </div>
                </div>
                <button type="submit" className="btn btn-primary">Update</button>
            </form>

        )
    }

}

export default ProductUpdate