const jwt = require('jsonwebtoken')
const secret = 'del-express'

module.exports.verify = (req, res, next) => {
	let token = 'Bearer ' + req.headers.authorization

	if (typeof token == 'undefined'){
		res.status(200).json({ error: 'undefined-auth-header' })
	} else {
		let slicedToken = token.slice(7, token.length)
		
		jwt.verify(slicedToken, secret, (err, data) => {

			if (err) {
				if (err.name == 'TokenExpiredError') {
					res.status(200).json({ error: 'token-expired' })
				} else {
					res.status(200).json({ error: 'token-auth-failed' })
				}
			} else {
				next()
			}
		})
	}
}

module.exports.createToken = (user) => {
	let data = {
		_id: user._id,
		email: user.email,
		role: user.role,
		stripeCustomerId: user.stripeCustomerId
	}

	let token = jwt.sign(data, secret, { expiresIn: '2hrs' })
	console.log(token)

	return token

}

module.exports.getId = (authToken) => {
	return getPayload(authToken)._id
}

module.exports.getStripeCustomerId = (authToken) => {
	return getPayload(authToken).stripeCustomerId
}

getPayload = (authToken) => {
	return jwt.decode(authToken, { complete:true }).payload
}