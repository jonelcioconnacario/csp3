const mongoose = require('mongoose')
const Schema = mongoose.Schema

const BookingSchema = new Schema({

	shipperName: {
		type: String,
		required: [true, 'shipperName is required.']
	},
	userId: {
		type: String,
		required: [false]
	},
	productId: {
		type: String,
		required: [true, 'productId is required.']
	},
	amount: {
		type: String,
		required: [true, 'amount is required.']
	},
	cosgineeName: {
		type: String,
		required: [true, 'cosgineeName is required.']
	},
	cosigneeNumber: {
		type: String,
		required: [true, 'cosigneeNumber is required.']
	},
	streetName: {
		type: String,
		required: [true, 'streetName is required.']
	},
	barangay: {
		type: String,
		required: [true, 'barangay is required.']
	},
	cityMuni: {
		type: String,
		required: [true, 'cityMuni is required.']
	},
	regionProvince: {
		type: String,
		required: [true, 'regionProvince is required.']
	},
	zipcode: {
		type: String,
		required: [true, 'zipcode is required.']
	},
	itemDescription: {
		type: String,
		itemDescription: [true, 'itemDescription is required.']
	},
	specialInstruction: {
		type: String,
		specialInstruction: [true, 'specialInstruction is required.']
	},
})

const Booking = mongoose.model('booking', BookingSchema)

module.exports = Booking