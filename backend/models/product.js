const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ProductSchema = new Schema({

	ProductName: {
		type: String,
		required: [true, 'ProductName is required.']
    },
    SmallAmount: {
		type: Number,
		required: [true, 'SmallAmount is required.']
    },
    MediumAmount: {
		type: Number,
		required: [true, 'MediumAmount is required.']
    },
    LargeAmount: {
		type: Number,
		required: [true, 'LargeAmount is required.']
    },
    isArchived: {
        type: Boolean,
		required: [true, 'Archive status is required.']
    }

})

const product = mongoose.model('product', ProductSchema)

module.exports = product