const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema({
	name: {
		type: String,
		required: [true, 'Name is required.']
	},
	email: {
		type: String,
		required: [true, 'Email is required.']
	},
	password: {
		type: String,
		required: [true, 'Password is required.']
	},
	role: {
		type: String,
		default: 'customer'
	},
	stripeCustomerId: {
		type: String,
		required: [true, 'Stripe customer ID is required.	']
	},
	booking: [
	{
		bookingId: {
			type: String,
			required: [true, 'Booking ID is required.']
		},
		dateTimeRecorded: {
			type: Date,
			default: new Date()
		},
		paymentMode: {
			type: String,
			required: [true, 'Payment Mode is required.']
		},
		totalPrice: {
			type: Number,
			required: [true, 'Total Price is required.']
		},
		stripeChargeId: {
			type: String,
			default: null
		}
	}
	]
}, {
	timestamps: true
})

const User = mongoose.model('user', UserSchema)

module.exports = User



