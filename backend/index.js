// Declare constants.
const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')


// Declae constansts
const app = express()
const defaultPort = 4000
const designatedPort = process.env.port || defaultPort


// Enable the Cross Origin Resource Sharing (CORS)
app.use(function(req, res, next) {
res.header('Access-Control-Allow-Origin', '*')
res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
next()
})

app.use(express.static('public'));

// Declare database connection

mongoose.connect('mongodb://localhost/del-express', {
	urlNewUrlParser: true,
	useCreateIndex: true
})


// Declare middlewares.
app.use(bodyParser.json())


// Declare routes.
app.use('/api', require('./routes'))


// Listen to server requests.
app.listen(designatedPort, () => {
console.log('Node.js backend now online in port ' + designatedPort)
})  