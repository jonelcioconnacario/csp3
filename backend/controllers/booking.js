const Booking = require('../models/booking')
const User = require('../models/user')
const auth = require('../jwt-auth')
const stripe = require('stripe')('sk_test_iqvEjhx2zZoHvrIyST8GCEgV00dttlioYV')

module.exports.create = (req, res) => {
    let userId = auth.getId(req.headers.authorization)

    // console.log(userId)

    let bookingItems = [{
        shipperName: req.body.shipperName,
        userId: userId,
        productId: req.body.productId,
        amount: req.body.totalPrice,
        cosgineeName: req.body.cosgineeName,
        cosigneeNumber: req.body.cosigneeNumber,
        streetName: req.body.streetName,
        barangay: req.body.barangay,
        cityMuni: req.body.cityMuni,
        regionProvince: req.body.regionProvince,
        zipcode: req.body.zipcode,
        itemDescription: req.body.itemDescription,
        specialInstruction: req.body.specialInstruction
    }]

    Booking.create(bookingItems).then((bookings) => {

        User.findOne({ _id: userId }).exec().then((user) => {
            let newBooking = user.booking.create({
                datetimeRecorded: new Date(),
                paymentMode: 'Cash on Delivery',
                totalPrice: req.body.totalPrice,
                bookingId: bookings[0]._id
            })
            user.booking.push(newBooking)
            user.save()

            res.status(200).json({ result: 'success' })
        }).catch((err) => {
            res.status(400).json({ error: err.message })
        })

    }).catch((err) => {
        res.status(400).json({ error: err.message })
    })

}

module.exports.stripeCheckout = (req, res) => {
    let userId = auth.getId(req.headers.authorization)

    console.log(userId)

    let bookingItems = [{
        shipperName: req.body.shipperName,
        userId: userId,
        productId: req.body.productId,
        amount: req.body.totalPrice,
        cosgineeName: req.body.cosgineeName,
        cosigneeNumber: req.body.cosigneeNumber,
        streetName: req.body.streetName,
        barangay: req.body.barangay,
        cityMuni: req.body.cityMuni,
        regionProvince: req.body.regionProvince,
        zipcode: req.body.zipcode,
        itemDescription: req.body.itemDescription,
        specialInstruction: req.body.specialInstruction
    }]

    Booking.create(bookingItems).then((bookings) => {

        stripe.charges.create({
            amount: req.body.totalPrice * 100,
            description: 'Online Payment',
            currency: 'php',
            customer: auth.getStripeCustomerId(req.headers.authorization)

        }, (err, charge) => {
            if (err) { return res.status(400).json({ error: err.message }) }

            User.findOne({ _id: userId }).exec().then((user) => {
                let newBooking = user.booking.create({
                    datetimeRecorded: new Date(),
                    paymentMode: 'Stripe',
                    totalPrice: req.body.totalPrice,
                    bookingId: bookings[0]._id,
                    stripeChargeId: charge.id
                })
                user.booking.push(newBooking)
                user.save()

                res.status(200).json({ result: 'success' })
            }).catch((err) => {
                res.status(400).json({ error: err.message })
            })

        })


    }).catch((err) => {
        res.status(400).json({ error: err.message })
    })

    
}

module.exports.all = (req, res) => {
    Booking.find().then((lists) => {
		res.status(200).json(lists)
	}).catch((err) => {
		res.status(400).json({ error: err.message })
	})

 }