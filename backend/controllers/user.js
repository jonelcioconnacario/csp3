// module.exports.login = (req, res) => {res.send('POST request detected at /api/user/login path.')}
// module.exports.register = (req, res) => {res.send('POST request detected at /api/user/register path.')}

const User = require('../models/user')
const bcrypt = require('bcrypt')
const stripe = require('stripe')('sk_test_iqvEjhx2zZoHvrIyST8GCEgV00dttlioYV')
const auth = require('../jwt-auth')


var SALT_WORK = 10

module.exports.login = (req, res) => {
	let condition = { email: req.body.email }

	User.findOne(condition).exec().then((user) => {
		if (user == null){
			res.status(200).json({ error: 'No user found, please try again' })
		}else{
			bcrypt.compare(req.body.password, user.password, (err, result) => {
				if (err) { return res.status(400).json({ error: err.message }) }

				if (result){
					res.status(200).json({
						result: 'authenticated',
						role: user.role,
						name: user.name,
						token: auth.createToken(user)
					})
				}else{
					res.status(400).json({ error: 'No result found.' })
				}
			})
		}
	}).catch((err) => {
		res.status(400).json({ error: err.message })
	})

}

module.exports.register = (req, res) => {
	bcrypt.genSalt(SALT_WORK, function(err, salt) {
		if (err) return next(err);

		bcrypt.hash(req.body.password, salt, (err, hash) => {

			if (err) { return res.status(400).json({ error: err.message }) }

				stripe.customers.create({
					email: req.body.email,
					source: 'tok_mastercard'
				}, (err,customer) => {
					if (err) { return res.status(400).json({ error: err.message }) }

					req.body.stripeCustomerId = customer.id
					req.body.password = hash


					User.create(req.body).then((user) => {
						res.status(200).json({ result: 'success' })
					}).catch((err) => {
						res.status(400).json({ error: err.message})
					})
				})
		})
	})
}

// module.exports.look = (req, res) => {
// 	let userId = auth.getId(req.headers.authorization)


//     User.find({_id: userId}).then((response) => {
// 		console.log(response)

// 		res.status(200).json(response)
// 	}).catch((err) => {
// 		res.status(400).json({ error: err.message})
// 	})
//  }
