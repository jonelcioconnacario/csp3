const Product = require('../models/product')

module.exports.create = (req, res) => {
    Product.create(req.body).then(() => {
		res.status(200).json({ result: 'success' })
	}).catch((err) => {
		res.status(400).json({ error: err.message})
	})
}

module.exports.all = (req, res) => {
    Product.find({isArchived: false}).then((products) => {
		res.status(200).json(products)
	}).catch((err) => {
		res.status(400).json({ error: err.message})
	})
}

module.exports.delete = (req, res) => {
	let searchParam = { _id: req.body._id }
	let updateParam = { isArchived: true }

	Product.findOneAndUpdate(searchParam, updateParam, (product) => {
		res.status(200).json({ result: 'success' })
	}).catch((err) =>{
		res.status(400).json({ error: err.message })
	})
}

module.exports.detail = (req, res) => {

	Product.findOne({_id: req.params._id}, ).then((product) => {
		res.status(200).json(product)
	}).catch((err) => {
		res.status(400).json({ error: err.message })
	})
}

module.exports.update = (req, res) => {
	let searchParam = { _id: req.body._id }
	let updateParams = req.body
	console.log("here")
	Product.findOneAndUpdate(searchParam, updateParams, (product) => {
		res.status(200).json({ result: 'success' })
	}).catch((err) =>{
		res.status(400).json({ error: err.message })
	})
}