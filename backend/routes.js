const express = require('express')
const router = express.Router()
const auth = require('./jwt-auth')

const UserController = require('./controllers/user')
const BookingController = require('./controllers/booking')
const ProductController = require('./controllers/product')

router.post('/user/register', UserController.register)
router.post('/user/login', UserController.login)
// router.get('/user/look', UserController.look)


router.post('/booking/create', auth.verify,BookingController.create)
router.post('/booking/stripeCheckout', auth.verify,BookingController.stripeCheckout)
router.get('/booking/all', BookingController.all)

router.post('/product/create', ProductController.create)
router.get('/product/all', ProductController.all)
router.delete('/product/delete', ProductController.delete)
router.get('/product/update/:_id', ProductController.detail)
router.put('/product', ProductController.update)

module.exports = router